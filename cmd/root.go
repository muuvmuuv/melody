package cmd

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string // our config file path
	dirPath string // project directory path

	rootCmd = &cobra.Command{
		Version: "0.0.0",
		Use:     "melody",
		Short:   "An enhanced git flow for GitLab",
		Long: `You can see melody as an enhanced git flow that
extends Vincent Driessen´s git-flow to work with GitLab.`,
	}
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(
		changeDir,
		initConfig,
		setLogLevel,
	)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default \"melody.toml\")")
	rootCmd.PersistentFlags().StringVar(&dirPath, "path", ".", "project directory")
}

func changeDir() {
	err := os.Chdir(dirPath)
	if err != nil {
		log.Panic(err)
	}
}

func initConfig() {
	viper.AddConfigPath(".")
	viper.SetConfigType("toml")
	viper.SetConfigName("melody")

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	}

	viper.AutomaticEnv()

	viper.SetDefault("log_level", 4)

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Fatal("Config file not found. Please create a melody.toml file in your root.")
		} else {
			log.Fatal(fmt.Errorf("Fatal error config file: %s", err))
		}
	}

	log.Debug("Using config file: ", viper.ConfigFileUsed())
}

func setLogLevel() {
	if logLevel := log.Level(viper.GetInt32("log_level")); logLevel >= 0 {
		log.SetLevel(logLevel)
		log.Debug("Set log level to ", logLevel)
	}
}
