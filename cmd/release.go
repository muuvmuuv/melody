package cmd

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/muuvmuuv/melody/project"
	"gitlab.com/muuvmuuv/melody/utils"
)

func init() {
	rootCmd.AddCommand(command)
}

var command = &cobra.Command{
	Use:   "release",
	Short: "Start a new release",
	Long:  `This will do the following things...`,
	Run: func(cmd *cobra.Command, args []string) {
		// Check if in main branch
		mainBranch := viper.GetString("main_branch")
		if isMain := utils.CurrentBranchIs(mainBranch); !isMain {
			log.Fatal(fmt.Errorf("You cannot start a release outside your main branch, you are on \"%s\"", mainBranch))
		}

		// Look which vcs we are using to continue
		var vcs string
		var vcsOpt *viper.Viper
		if gitlab := viper.Sub("gitlab"); gitlab != nil {
			vcs = "gitlab"
			vcsOpt = gitlab
		} else {
			log.Fatal("No VCS configuration found, but at least one is needed")
		}
		log.Debug("VCS name: ", vcs)
		vcsProjectID := vcsOpt.GetInt("project_id")
		log.Debug("VCS project ID: ", vcsProjectID)

		// Get the correct project
		project, err := project.GetProject()
		if err != nil {
			log.Fatal(err)
		}
		log.Debug("Project type: ", project.Name)
		currentVersion := project.GetVersion()
		log.Debug("Current version: ", currentVersion.String())

		// Ask which version should be next
		newVersion := utils.SelectNewVersion(currentVersion)
		println(newVersion.String())

		// TODO create changelog with
	},
}
