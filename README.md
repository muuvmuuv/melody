# Melody

Read [IDEA.md](./IDEA.md)

- [Support](#support)
  - [Project types](#project-types)
  - [Version control systems](#version-control-systems)
- [Reads](#reads)

## Support

### Project types

- Angular
- Go

### Version control systems

- GitLab

## Reads

- https://github.com/petervanderdoes/gitflow-avh/blob/develop/git-flow-release
- https://semver.org/
- https://danielkummer.github.io/git-flow-cheatsheet/
- https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/
