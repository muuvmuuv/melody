package project

import (
	"errors"
	"sort"

	"github.com/Masterminds/semver"
)

// Project API.
type Project struct {
	Name       string                  // capitalized name of the project, e.g. Angular or Go
	Website    string                  // website to the project homepage, not GitHub
	Repository string                  // repository website, e.g. https://github.com/angular/angular
	IsProject  func() bool             // a check that applies to 100% for this project type
	GetVersion func() *semver.Version  // receive the version for this type, like in Node from package.json
	SetVersion func(newVersion string) // set a new version for this type, e.g. in Node (npm) in package.json and package-lock.json
	priority   int                     // prevents e.g. Node project to be select in an Angular workspace
}

// Projects holds all project API´s.
type Projects struct {
	items   []*Project
	current string
}

var (
	projects = &Projects{}
)

// AddProject adds a new project.
func (p *Projects) AddProject(project *Project) {
	p.items = append(p.items, project)
}

// GetProject returns the current project and its functions.
func GetProject() (*Project, error) {
	sort.Slice(projects.items, func(i, j int) bool {
		return projects.items[i].priority > projects.items[j].priority
	})
	for i := 0; i < len(projects.items); i++ {
		project := projects.items[i]
		if isP := project.IsProject(); isP {
			return project, nil
		}
	}
	return &Project{}, errors.New("Your project is not yet supported, please post an issue on GitLab with your project details")
}
