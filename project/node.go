package project

import (
	"fmt"

	"github.com/Masterminds/semver"
	log "github.com/sirupsen/logrus"
	"gitlab.com/muuvmuuv/melody/utils"
)

func init() {
	projects.AddProject(nodeProject)
}

var nodeProject = &Project{
	Name:       "Node",
	Website:    "https://nodejs.org/",
	Repository: "https://github.com/nodejs/node",
	IsProject: func() bool {
		checks := []bool{
			utils.FileExists("package.json"),
		}
		return utils.SliceAllBool(checks, true)
	},
	GetVersion: func() *semver.Version {
		v, err := semver.NewVersion("1.0.0")
		if err != nil {
			log.Fatal(fmt.Errorf("Error parsing version: %s", err))
		}
		return v
	},
	SetVersion: func(newVersion string) {
		v, err := semver.NewVersion(newVersion)
		if err != nil {
			log.Fatal(fmt.Errorf("Error parsing version: %s", err))
		}
		println("Set version", v.String())
	},
}
