package project

import (
	"fmt"
	"strings"

	"github.com/Masterminds/semver"
	log "github.com/sirupsen/logrus"
	"gitlab.com/muuvmuuv/melody/utils"
)

func init() {
	projects.AddProject(goProject)
}

var goProject = &Project{
	Name:       "Go",
	Website:    "https://golang.org/",
	Repository: "https://github.com/golang/go",
	IsProject: func() bool {
		checks := []bool{
			utils.FileExists("go.mod"),
		}
		return utils.SliceAllBool(checks, true)
	},
	GetVersion: func() *semver.Version {
		lVersion := utils.LatestVersionTag()
		lVersion = strings.Replace(lVersion, "refs/tags/", "", -1)
		v, err := semver.NewVersion(lVersion)
		if err != nil {
			log.Fatal(fmt.Errorf("Error parsing version: %s", err))
		}
		return v
	},
	SetVersion: func(newVersion string) {
		// Versions are not stored in a file rather tags through VCS.
	},
}
