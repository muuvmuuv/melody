# Release

1. Check if in develop branch

   ```bash
   git branch --show-current
   ```

2. Fetch remote branch if updates are available
3. Create release branch

   ```bash
   git checkout -b release/0.1.0
   ```

4. Generate latest changelog
5. Bump version
6. Commit changes
7. Upload release Branch
8. Create a merge request
9. Delete release branch

## Visual

<figure style="margin:0;">
  <img src="https://wac-cdn.atlassian.com/dam/jcr:a9cea7b7-23c3-41a7-a4e0-affa053d9ea7/04%20(1).svg?cdnVersion=1342" width="100%" alt="">
  <caption><small>
    https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow
  </small></caption>
</figure>
