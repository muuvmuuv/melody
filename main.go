package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/muuvmuuv/melody/cmd"
)

func main() {
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "15:04:05",
	})
	log.SetLevel(log.InfoLevel)

	// Start program
	cmd.Execute()
}
