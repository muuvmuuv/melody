package utils

import "os"

// FileExists tells you if this file exists.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// SliceAllBool tells you if all slice items are of bool.
func SliceAllBool(a []bool, b bool) bool {
	for i := 0; i < len(a); i++ {
		if a[i] != b {
			return false
		}
	}
	return true
}
