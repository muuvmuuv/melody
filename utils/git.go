package utils

import (
	"strings"

	goGit "github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

var repo, _ = goGit.PlainOpen(".")

// CurrentBranchIs checks if the current branch is the given branch.
func CurrentBranchIs(name string) bool {
	head, _ := repo.Head()
	current := head.Name().String()
	return strings.Contains(current, name)
}

// LatestVersionTag returns the latest tag that is a version tag.
func LatestVersionTag() string {
	identifier := "v" // TODO: make this an option
	lVTag := ""

	tags, _ := repo.Tags()
	tags.ForEach(func(tag *plumbing.Reference) error {
		tagName := tag.Name().String()
		if l := strings.Contains(tagName, identifier); l {
			lVTag = tagName
		}
		return nil
	})

	return lVTag
}
