package utils

import (
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/Masterminds/semver"
	"github.com/manifoldco/promptui"
)

type inc struct {
	Name    string
	Version string
}

// SelectNewVersion interactively asks you how to increase a given version
func SelectNewVersion(version *semver.Version) *semver.Version {
	patchV := version.IncPatch()
	minorV := version.IncMinor()
	majorV := version.IncMajor()

	incList := []inc{
		{Name: "Patch", Version: patchV.String()},
		{Name: "Minor", Version: minorV.String()},
		{Name: "Major", Version: majorV.String()},
		{Name: "Other", Version: "select to choose"},
	}

	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}",
		Active:   "❯ {{ .Name | cyan }} ({{ .Version }})",
		Selected: "{{ . }}",
		Inactive: "  {{ .Name | cyan }} ({{ .Version }})",
		Details: `--------- Selected ----------
{{ "Name:" | faint }}	{{ .Name }}
{{ "Version:" | faint }}	{{ .Version }}`,
	}

	prompt := promptui.Select{
		Label:        "Select new version",
		Items:        incList,
		Templates:    templates,
		HideSelected: true,
		Size:         10,
	}

	i, _, err := prompt.Run()

	if err != nil {
		log.Fatal(fmt.Errorf("Prompt failed %v", err))
	}

	result := incList[i]

	if result.Name == "Other" {
		prompt := promptui.Prompt{
			Label:       "Custom version",
			HideEntered: true,
			Validate: func(input string) error {
				_, err := semver.NewVersion(input)
				if err != nil {
					return errors.New("This is not a valid version")
				}
				return nil
			},
		}
		oResult, err := prompt.Run()
		if err != nil {
			log.Fatal(fmt.Errorf("Prompt failed %v", err))
		}
		result.Version = oResult
	}

	log.Info(fmt.Sprintf("Increased by %s from %s to %s", result.Name, version.String(), result.Version))

	newVersion, err := semver.NewVersion(result.Version)
	if err != nil {
		log.Fatal(fmt.Errorf("Error parsing version: %s", err))
	}

	return newVersion
}
